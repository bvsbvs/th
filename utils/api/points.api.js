import { complementRequest } from '@/utils/http'
import { getUserJWToken } from '@/utils/user'

const URL_POINTS = '/api1/transithub/localities'

export const getPoints = async function(
  limit = null,
  offset = null,
  kind = 1,
  countryCode = null,
  regionCode = null,
  districtCode = null
) {
  const {
    data: {
      status,
      count,
      items
    }
  } = await this.$axios(complementRequest({
    method: 'get',
    url: URL_POINTS,
    params: {
      access_token: getUserJWToken(this),
      limit,
      offset,
      kind,
      country_code: countryCode,
      region_code: regionCode,
      district_code: districtCode
    }
  }))

  const result = {
    status,
    count,
    items : []
  }

  if (status) {
    for (const item of items) {
      result.items.push({
        guid: item.guid,
        nameUa: item.name_ua,
        nameRu: item.name_ru,
        kind: item.kind_id,
        countryCode: item.country_code,
        regionCode: item.region_code,
        districtCode: item.district_code,
        descriptionUa: item.description_ua,
        descriptionRu: item.description_ru,
        koatuu: item.koatuu,
        lat: item.lat,
        lng: item.lng,
        localityTypeUa: item.locality_type_ua,
        localityTypeRu: item.locality_type_ru
      })
    }
  }

  return result
}

export const getPoint = async function(koatuu) {
  const {
    data: {
      status,
      items
    }
  } = await this.$axios(complementRequest({
    method: 'get',
    url: URL_POINTS,
    params: {
      access_token: getUserJWToken(this),
      koatuu
    }
  }))

  const result = {
    status,
    item: {}
  }

  if (status && items.length > 0) {
    const locale = this.store.state.locale
    const item = items[0]
    result.item.guid = item.guid
    result.item.name = ((locale === 'ua' ? item.name_ua : item.name_ru) || '').pCapitalizeAllFirstWords()
    result.item.kindId = item.kind_id
    result.item.kind = item.kind
    result.item.districtCode = item.district_code
    result.item.districtName = ((locale === 'ua' ? item.district_name_ua : item.district_name_ru) || '').pCapitalizeAllFirstWords()
    result.item.regionCode = item.region_code
    result.item.regionName = ((locale === 'ua' ? item.region_name_ua : item.region_name_ru) || '').pCapitalizeAllFirstWords()
    result.item.countryCode = item.country_code || ''
    result.item.countryName = ((locale === 'ua' ? item.country_name_ua : item.country_name_ru) || '').pCapitalizeFirstWord()
    result.item.description = ((locale === 'ua' ? item.description_ua : item.description_ru) || '').pCapitalizeFirstWord()
    result.item.koatuu = item.koatuu
    result.item.lat = item.lat || ''
    result.item.lng = item.lng || ''
    result.item.type = ((locale === 'ua' ? item.locality_type_ua : item.locality_type_ru) || '')
    result.item.description = ((locale === 'ua' ? item.description_ua : item.description_ru) || '').pCapitalizeAllFirstWords()
  }

  return result
}