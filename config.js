export default {
  ui: {
    pagination: {
      pageSize: 10,
      offset: 0,
      currentPage: 1
    },
    sorting: {
      direction: 'desc'
    }
  },
  backend: {
    logistics: {
      token: 'e0d22acfba67c35937f2e26c7b6344e6'
    }
  }
}
